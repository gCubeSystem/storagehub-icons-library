
# Changelog for Storage Hub Icons Library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.2.0] - 2023-03-30

- ported to git

## [v1.0.0] - 2018-05-22

- First release
